def call(){
    pipeline {
        agent any
    
        stages{
            stage(verify){
                steps{
                    sh './mvnw -v'
                }
            }
            stage(compile){
                steps{
                    sh './mvnw clean compile'
                }
            }
            stage(test){
                steps{
                    sh './mvnw test'
                    junit 'target/surefire-reports/*.xml'
                }
            }
	        stage(quality){
	            steps{
	                withSonarQubeEnv('sonarqube') {
			    	    sh './mvnw sonar:sonar -Pcoverage'
		    	    }
		        }
	        }
        }
        post{
            success{
                office365ConnectorSend color: '#00FF00', message: 'Notif du pipeline de Valentin', status: 'Build Success', webhookUrl: 'https://oddet.webhook.office.com/webhookb2/8a0e2f4b-c01e-47fd-b8ec-9ed23d8761cc@75727609-df27-489a-9c9d-495dacd487cd/JenkinsCI/ba046b2958ee42849efbc88355e492ae/d47dc24d-2fc7-46a5-8712-ec2644617056'
            }
            failure{
                office365ConnectorSend color: '#FF0000', message: 'Notif du pipeline de Valentin', status: 'Build Failed', webhookUrl: 'https://oddet.webhook.office.com/webhookb2/8a0e2f4b-c01e-47fd-b8ec-9ed23d8761cc@75727609-df27-489a-9c9d-495dacd487cd/JenkinsCI/ba046b2958ee42849efbc88355e492ae/d47dc24d-2fc7-46a5-8712-ec2644617056'
            }
        }
    }
}
